<?php

namespace Helium\TextMessage;

use \Illuminate\Notifications\Notification;

class TextMessageChannel
{
	/**
	 * Send the given notification.
	 *
	 * @param  mixed  $notifiable
	 * @param  \Illuminate\Notifications\Notification  $notification
	 * @return void
	 */
	public function send ($notifiable, Notification $notification)
	{
		$message = $notification->toText($notifiable);
		TextMessage::sendMessage($message['message'], $notifiable->phone);
	}
}
