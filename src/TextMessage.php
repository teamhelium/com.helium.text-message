<?php

namespace Helium\TextMessage;

use Exception;

class TextMessage
{
	private static $url = "https://admin.heliumservices.com/texts/";

	public static function sendMessage ($message, $phone, $group = null) {

		if (self::apiKeyExists()) {

			if (empty($phone) && empty($group)) {
				throw new Exception("Either message or group must be defined. Both cannot be empty.");
			} else if (!empty($phone) && !empty($group)) {
				throw new Exception("Either message or group must be defined. Both cannot be defined.");
			}

			if (empty($message)) {
				throw new Exception("Message is empty. Message must contain at least 1 character");
			}


			$action = "message/send/";
			$fields = array(
				'message' => urlencode($message),
			);

			if (!empty($phone)) {
				$fields['phone'] = $phone;
			}

			if (!empty($group)) {
				$fields['group'] = $group;
			}

			$result = json_decode(self::doPost($action, $fields));

			if (isset($result->status) && $result->status == 'ok') {
				return true;
			}
			else {
				return false;
			}

		}

		else {
			throw new Exception("Helium Text API Key is not defined. Text API Key must be defined to continue.");
		}
	}

	public static function apiKeyExists () {

		$apiKey = env('HELIUM_TEXT_API');

		if (!empty($apiKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	private static function doPost ($action, $fields) {

		$fields_string = "";
		$url = self::$url . $action . env('HELIUM_TEXT_API');

		//url-ify the data for the POST
		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		rtrim($fields_string, '&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);

		return $result;
	}
}